import { browser, by, element, promise } from 'protractor';

export class AppPage {
  navigateTo(): promise.Promise<string> {
    return browser.get('/');
  }

  getTopbarTitle(): promise.Promise<string> {
    return element(by.css('mat-toolbar mat-toolbar-row a')).getText();
  }
}
