# DragonflyAssessment

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.2.

## Notes

I am not displaying a thumbnail image or status for every event on the home page because this would be over 500 http requests just to get the data I need. I felt perfomance would take a major hit and that isn't something I would want to go into a production build. Also, I'm not able to write many e2e tests, since they would consistently fail due to the API being unreliable.

If you run into any issues or are unable to get the app to run, please let me know and I can help troubleshoot.

### Installation
To install, clone or download this project and run `yarn` or `npm install`.


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
