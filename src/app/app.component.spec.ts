import { createComponentFixture } from 'angular-testing-library';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  let component: AppComponent;
  const fixture = createComponentFixture({
    component: AppComponent
  });

  beforeEach(async () => {
    await fixture.compile();
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
