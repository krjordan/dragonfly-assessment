import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatSnackBarModule } from '@angular/material';
import { EffectsModule } from '@ngrx/effects';

import { effects } from './store';

@NgModule({
  imports: [CommonModule, MatSnackBarModule, EffectsModule.forFeature(effects)]
})
export class NotificationsModule {}
