import { NotificationEffects } from './notifications.effect';

export const effects: any[] = [NotificationEffects];

export * from './notifications.effect';
