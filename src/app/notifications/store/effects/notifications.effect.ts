import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import * as eventsActions from '../../../events/store/actions';
import * as notifyActions from '../actions';
import { Message } from '../../models/message.model';

@Injectable()
export class NotificationEffects {
  constructor(private actions$: Actions, private snackBar: MatSnackBar) {}

  @Effect()
  handleLoadEventsFailure$: Observable<Action> = this.actions$
    .pipe(
      ofType<eventsActions.LoadEventsFailure>(eventsActions.LOAD_EVENTS_FAILURE)
    )
    .pipe(
      map((action: eventsActions.LoadEventsFailure) => action.payload),
      map(payload => {
        const message = {
          text: `Encountered a network issue, retrying...`,
          type: 'error'
        };
        this.open(message, 10000);
        return new notifyActions.NotifySuccess();
      })
    );

  @Effect()
  handleGetEventStatusFailure$: Observable<Action> = this.actions$
    .pipe(
      ofType<eventsActions.GetEventStatusFailure>(
        eventsActions.GET_EVENT_STATUS_FAILURE
      )
    )
    .pipe(
      map((action: eventsActions.GetEventStatusFailure) => action.payload),
      map(payload => {
        const message = {
          text: `Encountered a network issue, retrying...`,
          type: 'error'
        };
        this.open(message, 3000);
        return new notifyActions.NotifySuccess();
      })
    );

  @Effect()
  handleLoadSelectedImageFailure$: Observable<Action> = this.actions$
    .pipe(
      ofType<eventsActions.LoadSelectedImageFailure>(
        eventsActions.LOAD_SELECTED_IMAGE_FAILURE
      )
    )
    .pipe(
      map((action: eventsActions.LoadSelectedImageFailure) => action.payload),
      map(payload => {
        const message = {
          text: 'Encountered a network issue, retrying...',
          type: 'error'
        };
        this.open(message, 3000);
        return new notifyActions.NotifySuccess();
      })
    );

  @Effect()
  handleUpdateEventStatusSuccess$: Observable<Action> = this.actions$
    .pipe(
      ofType<eventsActions.UpdateEventStatusSuccess>(
        eventsActions.UPDATE_EVENT_STATUS_SUCCESS
      )
    )
    .pipe(
      map(() => {
        const message = {
          text: 'Successfully saved the event status',
          type: 'success'
        };
        this.open(message, 10000);
        return new notifyActions.NotifySuccess();
      })
    );

  @Effect()
  handleUpdateEventStatus$: Observable<Action> = this.actions$
    .pipe(
      ofType<eventsActions.UpdateEventStatus>(eventsActions.UPDATE_EVENT_STATUS)
    )
    .pipe(
      map(() => {
        const message = {
          text: 'Attempting to save the status, please wait...',
          type: 'error'
        };
        this.open(message, 1000000);
        return new notifyActions.NotifySuccess();
      })
    );

  /**
   * @description
   * Open a new snackbar message
   */
  open(message: Message, duration: number = 5000): void {
    this.snackBar.open(message.text, 'Dismiss', {
      duration,
      panelClass: message.type
    });
  }
}
