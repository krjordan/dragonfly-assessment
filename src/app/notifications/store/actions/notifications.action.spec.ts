import * as fromActions from './notifications.action';

describe('Notification Actions', () => {
  describe('Notify', () => {
    it('should create an action', () => {
      const payload = { text: 'Success!', type: 'success' };
      const action = new fromActions.Notify(payload);

      expect({ ...action }).toEqual({
        type: fromActions.NOTIFY,
        payload
      });
    });
  });

  describe('NotifySuccess', () => {
    it('should create an action', () => {
      const action = new fromActions.NotifySuccess();

      expect({ ...action }).toEqual({
        type: fromActions.NOTIFY_SUCCESS
      });
    });
  });
});
