import { Action } from '@ngrx/store';
import { Message } from '../../models/message.model';

export const NOTIFY = '[NOTIFICATION] Notify';
export const NOTIFY_SUCCESS = '[NOTIFICATION] Notify Success';

export class Notify implements Action {
  readonly type = NOTIFY;

  constructor(public payload?: Message) {}
}

export class NotifySuccess implements Action {
  readonly type = NOTIFY_SUCCESS;
}

export type NotifyActions = Notify | NotifySuccess;
