import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MatToolbarModule } from '@angular/material';

import { NotificationsModule } from './notifications/notifications.module';

// Store
import { EffectsModule } from '@ngrx/effects';
import {
  RouterStateSerializer,
  StoreRouterConnectingModule
} from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { CustomSerializer, reducers } from './store';

import { environment } from '../environments/environment';

import { AppComponent } from './app.component';

// Routes
export const ROUTES: Routes = [
  {
    path: '',
    loadChildren: './events/events.module#EventsModule'
  }
];

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([]),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    RouterModule.forRoot(ROUTES, { onSameUrlNavigation: 'reload' }),
    StoreRouterConnectingModule.forRoot({
      stateKey: 'router'
    }),
    NotificationsModule
  ],
  providers: [{ provide: RouterStateSerializer, useClass: CustomSerializer }],
  bootstrap: [AppComponent]
})
export class AppModule {}
