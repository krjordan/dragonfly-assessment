import { createComponentFixture } from 'angular-testing-library';

import { SelectedEventCommentsComponent } from './selected-event-comments.component';

describe('SelectedEventCommentsComponent', () => {
  let component: SelectedEventCommentsComponent;
  const fixture = createComponentFixture({
    component: SelectedEventCommentsComponent
  });

  beforeEach(async () => {
    await fixture.compile();
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
