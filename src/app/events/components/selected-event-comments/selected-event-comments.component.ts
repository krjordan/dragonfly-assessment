import { Component, Input } from '@angular/core';
import { Comment } from '../../models/events.models';

@Component({
  selector: 'app-selected-event-comments',
  templateUrl: './selected-event-comments.component.html',
  styleUrls: ['./selected-event-comments.component.scss']
})
export class SelectedEventCommentsComponent {
  @Input()
  comments: Comment[];
}
