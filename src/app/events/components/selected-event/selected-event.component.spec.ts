import { createComponentFixture } from 'angular-testing-library';

import { SelectedEventComponent } from './selected-event.component';

describe('SelectedEventComponent', () => {
  let component: SelectedEventComponent;
  const fixture = createComponentFixture({
    component: SelectedEventComponent
  });

  beforeEach(async () => {
    await fixture.compile();
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
