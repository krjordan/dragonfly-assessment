import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Event, EventStatus } from '../../models/events.models';

@Component({
  selector: 'app-selected-event',
  templateUrl: './selected-event.component.html',
  styleUrls: ['./selected-event.component.scss']
})
export class SelectedEventComponent {
  @Input()
  event: Event;
  @Input()
  imageData: string;
  @Input()
  status: boolean;
  @Output()
  updateEventStatus: EventEmitter<EventStatus> = new EventEmitter<
    EventStatus
  >();

  /**
   * @description
   * Switches the status flag and emits the updateEventStatus event.
   */
  updateStatus(id: string): void {
    this.status = !this.status;
    this.updateEventStatus.emit({ eventId: id, coming: this.status });
  }
}
