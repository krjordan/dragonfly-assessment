import { createComponentFixture } from 'angular-testing-library';

import { EventCardComponent } from './event-card.component';

describe('EventCardComponent', () => {
  let component: EventCardComponent;
  const fixture = createComponentFixture({
    component: EventCardComponent
  });

  beforeEach(async () => {
    await fixture.compile();
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
