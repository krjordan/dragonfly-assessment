import { Component, Input } from '@angular/core';
import { Event } from '../../models/events.models';

@Component({
  selector: 'app-selected-event-detail',
  templateUrl: './selected-event-detail.component.html',
  styleUrls: ['./selected-event-detail.component.scss']
})
export class SelectedEventDetailComponent {
  @Input()
  event: Event;
  @Input()
  imageUrl: string;
}
