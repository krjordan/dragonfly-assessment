import { createComponentFixture } from 'angular-testing-library';

import { SafeUrlPipe } from '../../pipes/safe-url.pipe';
import { SelectedEventDetailComponent } from './selected-event-detail.component';

describe('SelectedEventDetailComponent', () => {
  let component: SelectedEventDetailComponent;
  const fixture = createComponentFixture({
    component: SelectedEventDetailComponent,
    declarations: [SafeUrlPipe]
  });

  beforeEach(async () => {
    await fixture.compile();
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
