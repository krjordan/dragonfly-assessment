export interface Event {
  id: string;
  name: string;
  description: string;
  images: Array<Image>;
  location: Location;
  date: string;
  comments: Array<Comment>;
}

export interface Image {
  id: string;
  caption: string;
}

export interface ImageRequest {
  eventId: string;
  mediaId: string;
}

export interface Location {
  name: string;
  address: string;
  city: string;
  state: string;
}

export interface Comment {
  from: string;
  text: string;
}

export interface EventStatus {
  eventId: string;
  coming: boolean;
}
