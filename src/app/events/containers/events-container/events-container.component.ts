import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { Event } from '../../models/events.models';

import * as fromEvents from '../../store';

@Component({
  selector: 'app-events-container',
  templateUrl: './events-container.component.html',
  styleUrls: ['./events-container.component.scss']
})
export class EventsContainerComponent implements OnInit {
  events$: Observable<Event[]>;
  eventsSub: Subscription;
  validEvents = true;

  constructor(
    private store: Store<fromEvents.EventsState>,
    private router: Router
  ) {}

  ngOnInit() {
    this.store.dispatch(new fromEvents.LoadEvents());
    this.events$ = this.store.pipe(select(fromEvents.getEvents));
    // Check to see whether the events are returned as an Array or a string
    if (this.events$) {
      this.eventsSub = this.events$.subscribe(events => {
        if (events && events instanceof Array) {
          this.validEvents = true;
        } else {
          this.validEvents = false;
        }
      });
    }
  }

  /**
   * @description
   * Dispatches an action to load the selected event into state
   * then navigates the app to that event's routes
   */
  updateSelectedEvent(event: Event): void {
    this.store.dispatch(new fromEvents.LoadSelectedEvent(event));
    this.router.navigate(['/events', event.id]);
  }
}
