import {
  createComponentFixture,
  provideMagicalMock
} from 'angular-testing-library';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { BehaviorSubject } from 'rxjs';
import { EventsContainerComponent } from './events-container.component';

describe('EventsContainerComponent', () => {
  let component: EventsContainerComponent;
  const fixture = createComponentFixture({
    component: EventsContainerComponent,
    providers: [provideMagicalMock(Store), provideMagicalMock(Router)]
  });

  const events$ = new BehaviorSubject<any>([]);

  beforeEach(async () => {
    await fixture.compile({
      events$
    });
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
