import {
  createComponentFixture,
  provideMagicalMock
} from 'angular-testing-library';
import { Store } from '@ngrx/store';

import { SelectedEventContainerComponent } from './selected-event-container.component';

describe('SelectedEventContainerComponent', () => {
  let component: SelectedEventContainerComponent;
  const fixture = createComponentFixture({
    component: SelectedEventContainerComponent,
    providers: [provideMagicalMock(Store)]
  });

  beforeEach(async () => {
    await fixture.compile();
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
