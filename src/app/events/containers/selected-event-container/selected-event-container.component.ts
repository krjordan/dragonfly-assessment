import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';

import * as fromEvents from '../../store';
import * as fromRouter from '../../../store';

import { Event, EventStatus } from '../../models/events.models';

@Component({
  selector: 'app-selected-event-container',
  templateUrl: './selected-event-container.component.html',
  styleUrls: ['./selected-event-container.component.scss']
})
export class SelectedEventContainerComponent implements OnInit, OnDestroy {
  events$: Observable<Event[]>;
  eventsSub: Subscription;
  selectedEvent$: Observable<Event>;
  selectedEventSub: Subscription;
  imageData$: Observable<string>;
  status$: Observable<boolean> = this.store.pipe(
    select(fromEvents.getSelectedEventStatus)
  );
  statusSub: Subscription;
  routerParamsId$: Observable<string>;
  routerParamsIdSub: Subscription;
  paramsId: string;

  constructor(private store: Store<fromEvents.EventsState>) {}

  ngOnInit() {
    this.selectedEvent$ = this.store.pipe(select(fromEvents.getSelectedEvent));
    if (this.selectedEvent$) {
      this.selectedEventSub = this.selectedEvent$.subscribe(event => {
        if (event) {
          this.store.dispatch(
            new fromEvents.LoadSelectedImage({
              eventId: event.id,
              mediaId: event.images[0].id
            })
          );
          this.store.dispatch(new fromEvents.GetEventStatus(event.id));
          if (this.status$) {
            this.statusSub = this.status$.subscribe(status => {
              if (typeof status !== 'boolean') {
                this.store.dispatch(new fromEvents.GetEventStatus(event.id));
              }
            });
          }
          this.imageData$ = this.store.pipe(
            select(fromEvents.getSelectedImage)
          );
        } else {
          this.store.dispatch(new fromEvents.LoadEvents());
          this.reloadSelectedEvent();
        }
      });
    }
  }

  ngOnDestroy(): void {
    if (this.selectedEventSub) {
      this.selectedEventSub.unsubscribe();
    }
    if (this.routerParamsIdSub) {
      this.routerParamsIdSub.unsubscribe();
    }
    if (this.eventsSub) {
      this.eventsSub.unsubscribe();
    }
    if (this.statusSub) {
      this.statusSub.unsubscribe();
    }
  }

  /**
   * @description
   * Takes the routerParamsId and compares it the array of events.
   * If the ids match, then the state is updated with the selected
   * event.
   */
  reloadSelectedEvent(): void {
    this.routerParamsId$ = this.store.pipe(
      select(fromRouter.getCurrentParamsId)
    );
    if (this.routerParamsId$) {
      this.routerParamsIdSub = this.routerParamsId$.subscribe(id => {
        this.paramsId = id;
      });
    }

    this.events$ = this.store.pipe(select(fromEvents.getEvents));
    if (this.events$) {
      this.eventsSub = this.events$.subscribe(events => {
        if (events && events instanceof Array) {
          events.filter(e => {
            if (e.id === this.paramsId) {
              this.store.dispatch(new fromEvents.LoadSelectedEvent(e));
              this.store.dispatch(new fromEvents.GetEventStatus(e.id));
              if (this.status$) {
                this.statusSub = this.status$.subscribe(status => {
                  if (typeof status !== 'boolean') {
                    this.store.dispatch(new fromEvents.GetEventStatus(e.id));
                  }
                });
              }
              this.imageData$ = this.store.pipe(
                select(fromEvents.getSelectedImage)
              );
            }
          });
        }
      });
    }
  }

  /**
   * @description
   * Dispatches an action to update the event status (e.g., coming: true | false)
   */
  updateStatus(data: EventStatus): void {
    this.store.dispatch(new fromEvents.UpdateEventStatus(data));
  }
}
