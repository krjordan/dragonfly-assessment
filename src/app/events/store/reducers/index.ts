import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';

import * as fromEvents from './events.reducer';

export interface EventsState {
  events: fromEvents.EventsState;
}

export const reducers: ActionReducerMap<EventsState> = {
  events: fromEvents.reducer
};

export const eventsState = createFeatureSelector<EventsState>('events');
