import * as eventsActions from '../actions';

export interface EventsState {
  loading: boolean;
  loaded: boolean;
  events: any;
  selectedEvent: any;
  selectedImage: string;
  selectedEventStatus: boolean;
}

export const initialState: EventsState = {
  loading: false,
  loaded: false,
  events: [],
  selectedEvent: null,
  selectedImage: '',
  selectedEventStatus: false
};

export function reducer(
  state: EventsState = initialState,
  action: eventsActions.EventsActions
): EventsState {
  switch (action.type) {
    case eventsActions.LOAD_EVENTS: {
      return {
        ...state,
        loading: true,
        loaded: false,
        events: []
      };
    }

    case eventsActions.LOAD_EVENTS_SUCCESS: {
      return {
        ...state,
        loading: false,
        loaded: true,
        events: action.payload
      };
    }

    case eventsActions.LOAD_SELECTED_EVENT: {
      return {
        ...state,
        selectedEvent: action.payload
      };
    }

    case eventsActions.LOAD_SELECTED_IMAGE: {
      return {
        ...state,
        selectedImage: ''
      };
    }

    case eventsActions.LOAD_SELECTED_IMAGE_SUCCESS: {
      return {
        ...state,
        selectedImage: action.payload
      };
    }

    case eventsActions.GET_EVENT_STATUS: {
      return {
        ...state,
        selectedEventStatus: false
      };
    }

    case eventsActions.UPDATE_EVENT_STATUS_SUCCESS: {
      return {
        ...state,
        selectedEventStatus: !state.selectedEventStatus
      };
    }

    case eventsActions.GET_EVENT_STATUS_SUCCESS: {
      return {
        ...state,
        selectedEventStatus: action.payload ? action.payload.coming : false
      };
    }

    default: {
      return state;
    }
  }
}

export const getEvents = (state: EventsState) => state.events;
export const getEventsLoaded = (state: EventsState) => state.loaded;
export const getSelectedEvent = (state: EventsState) => state.selectedEvent;
export const getSelectedImage = (state: EventsState) => state.selectedImage;
export const getSelectedEventStatus = (state: EventsState) =>
  state.selectedEventStatus;
