import * as fromEvents from './events.reducer';

describe('Events Reducer', () => {
  const { initialState } = fromEvents;

  describe('unknown action', () => {
    it('should have an initial state', () => {
      const action = {} as any;
      const state = fromEvents.reducer(undefined, action);

      expect(state).toBe(initialState);
    });
  });
});
