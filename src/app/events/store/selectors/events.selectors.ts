import { createSelector } from '@ngrx/store';

import * as fromFeature from '../reducers';
import * as fromEvents from '../reducers/events.reducer';

export const getEventsState = createSelector(
  fromFeature.eventsState,
  (state: fromFeature.EventsState) => state.events
);

export const getEvents = createSelector(getEventsState, fromEvents.getEvents);

export const getEventsLoaded = createSelector(
  getEventsState,
  fromEvents.getEventsLoaded
);

export const getSelectedEvent = createSelector(
  getEventsState,
  fromEvents.getSelectedEvent
);

export const getSelectedImage = createSelector(
  getEventsState,
  fromEvents.getSelectedImage
);

export const getSelectedEventStatus = createSelector(
  getEventsState,
  fromEvents.getSelectedEventStatus
);
