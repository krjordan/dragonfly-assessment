import { Action } from '@ngrx/store';
import { EventStatus, Event } from '../../models/events.models';

// Load Events
export const LOAD_EVENTS = '[Event] Load Events';
export const LOAD_EVENTS_SUCCESS = '[Event] Load Events Success';
export const LOAD_EVENTS_FAILURE = '[Event] Load Events Failure';

export class LoadEvents implements Action {
  readonly type = LOAD_EVENTS;
}

export class LoadEventsSuccess implements Action {
  readonly type = LOAD_EVENTS_SUCCESS;

  constructor(public payload: Event[]) {}
}

export class LoadEventsFailure implements Action {
  readonly type = LOAD_EVENTS_FAILURE;

  constructor(public payload?: any) {}
}

// Load Selected Event

export const LOAD_SELECTED_EVENT = '[Event] Load Selected Event';

export class LoadSelectedEvent implements Action {
  readonly type = LOAD_SELECTED_EVENT;

  constructor(public payload: Event) {}
}

// Load Selected Image
export const LOAD_SELECTED_IMAGE = '[Event] Load Selected Image';
export const LOAD_SELECTED_IMAGE_SUCCESS =
  '[Event] Load Selected Image Success';
export const LOAD_SELECTED_IMAGE_FAILURE =
  '[Event] Load Selected Image Failure';

export class LoadSelectedImage implements Action {
  readonly type = LOAD_SELECTED_IMAGE;

  constructor(public payload: any) {}
}

export class LoadSelectedImageSuccess implements Action {
  readonly type = LOAD_SELECTED_IMAGE_SUCCESS;

  constructor(public payload: any) {}
}

export class LoadSelectedImageFailure implements Action {
  readonly type = LOAD_SELECTED_IMAGE_FAILURE;

  constructor(public payload?: any) {}
}

// Event Status
export const GET_EVENT_STATUS = '[Event] Get Event Status';
export const GET_EVENT_STATUS_SUCCESS = '[Event] Get Event Status Success';
export const GET_EVENT_STATUS_FAILURE = '[Event] Get Event Status Failure';
export const UPDATE_EVENT_STATUS = '[Event] Update Event Status';
export const UPDATE_EVENT_STATUS_SUCCESS =
  '[Event] Update Event Status Success';
export const UPDATE_EVENT_STATUS_FAILURE =
  '[Event] Update Event Status Failure';

export class GetEventStatus implements Action {
  readonly type = GET_EVENT_STATUS;

  constructor(public payload: any) {}
}

export class GetEventStatusSuccess implements Action {
  readonly type = GET_EVENT_STATUS_SUCCESS;

  constructor(public payload: any) {}
}

export class GetEventStatusFailure implements Action {
  readonly type = GET_EVENT_STATUS_FAILURE;

  constructor(public payload?: any) {}
}

export class UpdateEventStatus implements Action {
  readonly type = UPDATE_EVENT_STATUS;

  constructor(public payload: EventStatus) {}
}

export class UpdateEventStatusSuccess implements Action {
  readonly type = UPDATE_EVENT_STATUS_SUCCESS;

  constructor(public payload: any) {}
}

export class UpdateEventStatusFailure implements Action {
  readonly type = UPDATE_EVENT_STATUS_FAILURE;

  constructor(public payload?: any) {}
}

export type EventsActions =
  | LoadEvents
  | LoadEventsSuccess
  | LoadEventsFailure
  | LoadSelectedEvent
  | LoadSelectedImage
  | LoadSelectedImageSuccess
  | LoadSelectedImageFailure
  | GetEventStatus
  | GetEventStatusSuccess
  | GetEventStatusFailure
  | UpdateEventStatus
  | UpdateEventStatusSuccess
  | UpdateEventStatusFailure;
