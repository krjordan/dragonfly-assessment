import * as fromEventsActions from './events.actions';
import { fromEvent } from 'rxjs';

describe('Events Actions', () => {
  describe('LoadEvents', () => {
    it('should create an action', () => {
      const action = new fromEventsActions.LoadEvents();

      expect({ ...action }).toEqual({
        type: fromEventsActions.LOAD_EVENTS
      });
    });
  });

  describe('LoadEventsSuccess', () => {
    it('should create an action', () => {
      const payload = [{ event: 'new event' }];
      const action = new fromEventsActions.LoadEventsSuccess(payload);

      expect({ ...action }).toEqual({
        type: fromEventsActions.LOAD_EVENTS_SUCCESS,
        payload
      });
    });
  });

  describe('LoadEventFailure', () => {
    it('should create an action', () => {
      const payload = { errorMessage: 'Load Error' };
      const action = new fromEventsActions.LoadEventsFailure(payload);

      expect({ ...action }).toEqual({
        type: fromEventsActions.LOAD_EVENTS_FAILURE,
        payload
      });
    });
  });

  describe('LoadSelectedImage', () => {
    it('should create an action', () => {
      const payload = '1234567890';
      const action = new fromEventsActions.LoadSelectedImage(payload);

      expect({ ...action }).toEqual({
        type: fromEventsActions.LOAD_SELECTED_IMAGE,
        payload
      });
    });
  });

  describe('LoadSelectedImageSuccess', () => {
    it('should create an action', () => {
      const payload = { image: 'image' };
      const action = new fromEventsActions.LoadSelectedImageSuccess(payload);

      expect({ ...action }).toEqual({
        type: fromEventsActions.LOAD_SELECTED_IMAGE_SUCCESS,
        payload
      });
    });
  });

  describe('LoadSelectedImageFailure', () => {
    it('should create an action', () => {
      const payload = { errorMessage: 'Load Error' };
      const action = new fromEventsActions.LoadSelectedImageFailure(payload);

      expect({ ...action }).toEqual({
        type: fromEventsActions.LOAD_SELECTED_IMAGE_FAILURE,
        payload
      });
    });
  });

  describe('GetEventStatus', () => {
    it('should create an action', () => {
      const payload = { coming: true };
      const action = new fromEventsActions.GetEventStatus(payload);

      expect({ ...action }).toEqual({
        type: fromEventsActions.GET_EVENT_STATUS,
        payload
      });
    });
  });

  describe('GetEventStatusSuccess', () => {
    it('should create an action', () => {
      const payload = {};
      const action = new fromEventsActions.GetEventStatusSuccess(payload);

      expect({ ...action }).toEqual({
        type: fromEventsActions.GET_EVENT_STATUS_SUCCESS,
        payload
      });
    });
  });

  describe('GetEventStatusFailure', () => {
    it('should create an action', () => {
      const payload = { errorMessage: 'error' };
      const action = new fromEventsActions.GetEventStatusFailure(payload);

      expect({ ...action }).toEqual({
        type: fromEventsActions.GET_EVENT_STATUS_FAILURE,
        payload
      });
    });
  });

  describe('UpdateEventStatusSuccess', () => {
    it('should create an action', () => {
      const payload = { eventId: '1234567890', coming: false };
      const action = new fromEventsActions.UpdateEventStatusSuccess(payload);

      expect({ ...action }).toEqual({
        type: fromEventsActions.UPDATE_EVENT_STATUS_SUCCESS,
        payload
      });
    });
  });

  describe('UpdateEventStatusFailure', () => {
    it('should create an action', () => {
      const payload = { errorMessage: 'Load Error' };
      const action = new fromEventsActions.UpdateEventStatusFailure(payload);

      expect({ ...action }).toEqual({
        type: fromEventsActions.UPDATE_EVENT_STATUS_FAILURE,
        payload
      });
    });
  });
});
