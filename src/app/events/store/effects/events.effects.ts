import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Action, Store, select } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { map, delay, exhaustMap, retryWhen } from 'rxjs/operators';

import * as eventsActions from '../actions/events.actions';
import * as fromEvents from '../index';
import { EventsService } from '../../services/events.service';

@Injectable()
export class EventsEffects {
  constructor(
    private actions$: Actions,
    private store: Store<fromEvents.EventsState>,
    private eventsService: EventsService
  ) {}

  @Effect()
  loadEvents$: Observable<Action> = this.actions$
    .pipe(ofType<eventsActions.LoadEvents>(eventsActions.LOAD_EVENTS))
    .pipe(
      exhaustMap(action =>
        this.eventsService.getEvents().pipe(
          map((res: any) => new eventsActions.LoadEventsSuccess(res)),
          retryWhen(stream => {
            this.store.dispatch(new eventsActions.LoadEventsFailure());
            return stream.pipe(
              delay(3000),
              map(err => of(new eventsActions.LoadEventsFailure(err)))
            );
          })
        )
      )
    );

  @Effect()
  getSelectedImage$: Observable<Action> = this.actions$
    .pipe(
      ofType<eventsActions.LoadSelectedImage>(eventsActions.LOAD_SELECTED_IMAGE)
    )
    .pipe(
      exhaustMap(action =>
        this.eventsService.getSelectedImage(action.payload).pipe(
          map(
            (res: any) =>
              new eventsActions.LoadSelectedImageSuccess(
                URL.createObjectURL(res)
              )
          ),
          retryWhen(stream => {
            this.store.dispatch(new eventsActions.LoadSelectedImageFailure());
            return stream.pipe(
              delay(3000),
              map(err => of(new eventsActions.LoadSelectedImageFailure(err)))
            );
          })
        )
      )
    );

  @Effect()
  getEventStatus$: Observable<Action> = this.actions$
    .pipe(ofType<eventsActions.GetEventStatus>(eventsActions.GET_EVENT_STATUS))
    .pipe(
      exhaustMap(action =>
        this.eventsService.getEventStatus(action.payload).pipe(
          map((res: any) => new eventsActions.GetEventStatusSuccess(res)),
          retryWhen(stream => {
            this.store.dispatch(new eventsActions.GetEventStatusFailure());
            return stream.pipe(
              delay(3000),
              map(err => of(new eventsActions.GetEventStatusFailure(err)))
            );
          })
        )
      )
    );

  @Effect()
  updateEventStatus$: Observable<Action> = this.actions$
    .pipe(
      ofType<eventsActions.UpdateEventStatus>(eventsActions.UPDATE_EVENT_STATUS)
    )
    .pipe(
      exhaustMap(action =>
        this.eventsService.updateEventStatus(action.payload).pipe(
          map((res: any) => new eventsActions.UpdateEventStatusSuccess(res)),
          retryWhen(stream =>
            stream.pipe(
              delay(5000),
              map(err => of(new eventsActions.UpdateEventStatusFailure(err)))
            )
          )
        )
      )
    );
}
