import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';
import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressHttpModule } from '@ngx-progressbar/http';

// Angular Material
import {
  MatCardModule,
  MatButtonModule,
  MatTabsModule,
  MatIconModule
} from '@angular/material';

// Store
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { effects, reducers } from './store';

// Services
import * as fromServices from './services';

// Containers
import { EventsContainerComponent } from './containers/events-container/events-container.component';
import { SelectedEventContainerComponent } from './containers/selected-event-container/selected-event-container.component';

// Components
import { EventCardComponent } from './components/event-card/event-card.component';
import { SelectedEventComponent } from './components/selected-event/selected-event.component';
import { SafeUrlPipe } from './pipes/safe-url.pipe';
import { SelectedEventDetailComponent } from './components/selected-event-detail/selected-event-detail.component';
import { SelectedEventCommentsComponent } from './components/selected-event-comments/selected-event-comments.component';

// Routes
export const ROUTES: Routes = [
  {
    path: '',
    component: EventsContainerComponent
  },
  {
    path: 'events/:id',
    component: SelectedEventContainerComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    MatCardModule,
    MatButtonModule,
    MatTabsModule,
    MatIconModule,
    RouterModule.forChild(ROUTES),
    StoreModule.forFeature('events', reducers),
    EffectsModule.forFeature(effects),
    NgProgressModule.forRoot(),
    NgProgressHttpModule.forRoot()
  ],
  providers: [...fromServices.services],
  declarations: [
    EventsContainerComponent,
    EventCardComponent,
    SelectedEventContainerComponent,
    SelectedEventComponent,
    SafeUrlPipe,
    SelectedEventDetailComponent,
    SelectedEventCommentsComponent
  ]
})
export class EventsModule {}
