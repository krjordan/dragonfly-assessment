import { inject, TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';

import { EventsService } from './events.service';

describe('EventsService', () => {
  let eventsService: EventsService;
  let httpMock: HttpTestingController;
  let http: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [EventsService]
    });
  });

  beforeEach(inject(
    [EventsService, HttpClient, HttpTestingController],
    (
      _eventsService_: EventsService,
      _http_: HttpClient,
      _httpMock_: HttpTestingController
    ) => {
      eventsService = _eventsService_;
      http = _http_;
      httpMock = _httpMock_;
    }
  ));

  it('should be defined', () => {
    expect(eventsService).toBeTruthy();
  });

  describe('getEvents', () => {
    const endpoint = '/api/events';
    let req: any;
    const returnData: Array<Object> = [{}];

    beforeEach(() => {
      eventsService.getEvents().subscribe();
      req = httpMock.expectOne(endpoint);
    });

    afterEach(() => {
      req.flush(returnData);
      httpMock.verify();
    });

    it('should perform a GET request', () => {
      expect(req.request.method).toEqual('GET');
    });

    it(`should send a request to ${endpoint}`, () => {
      expect(req.request.url).toBe(endpoint);
    });

    it('should have an empty body', () => {
      expect(req.request.body).toBeNull();
    });
  });

  describe('updateEventStatus', () => {
    const endpoint = '/api/events/123456789/status/blah';
    let req: any;
    const returnData: Array<Object> = [{}];

    beforeEach(() => {
      eventsService
        .updateEventStatus({ eventId: '123456789', coming: true })
        .subscribe();
      req = httpMock.expectOne(endpoint);
    });

    afterEach(() => {
      req.flush(returnData);
      httpMock.verify();
    });

    it('should perform a PUT request', () => {
      expect(req.request.method).toEqual('PUT');
    });

    it(`should send a request to ${endpoint}`, () => {
      expect(req.request.url).toBe(endpoint);
    });

    it('should have an empty body', () => {
      expect(req.request.body).toBeDefined();
    });
  });
});
