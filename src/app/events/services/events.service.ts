import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { EventStatus, ImageRequest } from '../models/events.models';

@Injectable({
  providedIn: 'root'
})
export class EventsService {
  constructor(private http: HttpClient) {}
  private _userName = 'blah';
  private _credentials = `${this._userName}:evalpass`;

  /**
   * @description
   * Gets a list of events
   */
  getEvents(): Observable<HttpResponse<{}>> {
    const headers = new HttpHeaders().set(
      'Authorization',
      `Basic ${btoa(this._credentials)}`
    );

    return this.http.get<HttpResponse<{}>>('/api/events', { headers });
  }

  /**
   * @description
   * Get Specific Media (e.g., picture)
   */
  getSelectedImage(data: ImageRequest): Observable<Blob> {
    const headers = new HttpHeaders().set(
      'Authorization',
      `Basic ${btoa(this._credentials)}`
    );

    return this.http.get<Blob>(
      `/api/events/${data.eventId}/media/${data.mediaId}`,
      {
        headers,
        responseType: 'blob' as any
      }
    );
  }

  /**
   * @description
   * Get status for a specific event
   */
  getEventStatus(eventId: string): Observable<HttpResponse<{}>> {
    const headers = new HttpHeaders().set(
      'Authorization',
      `Basic ${btoa(this._credentials)}`
    );

    return this.http.get<HttpResponse<{}>>(
      `/api/events/${eventId}/status/${this._userName}`,
      { headers }
    );
  }

  /**
   * @description
   * Save status for a specific event
   */
  updateEventStatus(data: EventStatus): Observable<HttpResponse<{}>> {
    const headers = new HttpHeaders().set(
      'Authorization',
      `Basic ${btoa(this._credentials)}`
    );

    return this.http.put<HttpResponse<{}>>(
      `/api/events/${data.eventId}/status/${this._userName}`,
      { coming: data.coming },
      { headers }
    );
  }
}
