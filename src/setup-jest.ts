import 'jest-preset-angular';

const mock = () => {
  let storage = {};
  return {
    getItem: key => (key in storage ? storage[key] : null),
    setItem: (key, value) => (storage[key] = value || ''),
    removeItem: key => delete storage[key],
    clear: () => (storage = {})
  };
};

Object.defineProperty(window, 'localStorage', { value: mock() });
Object.defineProperty(window, 'sessionStorage', { value: mock() });
Object.defineProperty(window, 'getComputedStyle', {
  value: () => ['-webkit-appearance']
});

// Used to fix the ReferenceError: CSS is not defined ERROR
Object.defineProperty(window, 'CSS', { value: mock() });
// Suppresses console warning from Angular Material
console.warn = () => {
  return;
};

Object.defineProperty(window, 'SVGElement', { value: Element });

/**
 * ISSUE: https://github.com/angular/material2/issues/7101
 * Workaround for JSDOM missing transform property
 */
Object.defineProperty(document.body.style, 'transform', {
  value: () => {
    return {
      enumerable: true,
      configurable: true
    };
  }
});

/**
 * Fixes Material Snackbar window binding
 */
Object.defineProperty(window, 'matchMedia', {
  value: () => {
    return window.matchMedia.bind(window);
  }
});
